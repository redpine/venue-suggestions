# Venue Suggestions

This project tracks feature suggestions from RedPine venues. Venues are encouraged to collaborate in the discussion, so let's work together to make the best platform possible!

This is not a full list of ongoing developments at RedPine, but a forum to guarantee that we build exactly the tools you need to run your business.


<-- Check out the "Issues" tab to see all currently requested features, and who else is interested in them.

Comment and request freely! We will moderate the issues periodically, but mostly for organization, aggregation, and/or cleanup.